## All channels:
### GET - https://api.system.pmecology.com/v1/data/<Access key>
#### Parameters:
* **timestamp** - The timestamp of the first value you want to request. The API will return the first **records** number of records in chronological order, starting with **timestamp**. It's in ISO 8601 format.
* **records**  - How many records to return in the request.
#### Values:
* **type** - Always "multi_channel".
* **hardware_id** - Hardware ID of the logger. Used to set up a logger in system.pmecology.com.
* **logger_name** - Name of the logger in system.pmecology.com dashboard.
* **first_record_timestamp** - Timestamp of the oldest record that is available. It's in ISO 8601 format.
* **last_record_timestamp** - Timestamp of the newest record that is available. It's in ISO 8601 format.
* **channels** - A dictionary of channels consisting of a channel number and an object. The object items are as follows:
  * **name** - Name of the channel.
  * **unit** - Unit of the channel.
  * **formula** - Formula that is applied to the values on this channel. If this value is missing, no formula is applied - **raw** and **value** are exactly the same.
* **history** - An array or objects with requested historical data. All records are in chronological order and their timestamps are >= **timestamp**.
  * **timestamp** - Time of creation of this particular data record. It's in ISO 8601 format.
  * **values_raw** - A dictionary of raw channel values (a value in mA, V, pulses or received directly from a digital sensor). Index is the channel number.
  * **values** - A dictionary of values recalculated with **formula**. Index is the channel number.
#### Example:
https://api.system.pmecology.com/v1/data/fc669aeb-55f6-4859-82c9-3d0244995455?timestamp=2019-01-07T13:48:01

## Creating an API key in PMEcology System
To create an API key you need to:
* Go to **Dashboard->Management->Settings->Data sharing**
![](http://db.wodkan.pmecology.com/management.settings.png)
* Select the channel and click **Create access key**, then select **All**.
![](http://db.wodkan.pmecology.com/new.access.key.png)
* Afterwards extract the **Access key** from the access key URL. For example, for: https://system.pmecology.com/?Key=bf567904-5005-46a7-ba94-310b557761a5#/location/f0946643f1ed41a0be9081df4e4d4781/data/channel/4

  The extracted key is: **bf567904-5005-46a7-ba94-310b557761a5**
  And the complete URL is: https://api.system.pmecology.com/v1/data/bf567904-5005-46a7-ba94-310b557761a5?timestamp=2019-01-01T00:00:00&records=1
